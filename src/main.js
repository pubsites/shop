import { createApp } from 'vue'
import App from './App.vue'
import 'materialize-css'
import 'materialize-css/dist/css/materialize.min.css'
// import 'material-design-icons/iconfont/material-icons.css'
import Router from './router'
createApp(App).use(Router).mount('#app')
