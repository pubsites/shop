import { createRouter, createWebHistory } from "vue-router";

const routes = [
    {
        path:'/',
        name:'Home',
        component:()=>import("@/components/pages/HomePage.vue"),
        props:{
            pTitle:"Certifications",
            msg:"Welcome to H.Ibrahimy portfolio app",
        }
    },
    {
        path:'/login',
        name:'SignIn',
        component:()=>import("@/components/pages/SignIn.vue"),
        props:{
            pTitle:"LOGIN",
            msg:"Please make sure to type your login credentials correctly!",
        }
    },
    {
        path:'/signup',
        name:'SignUp',
        component:()=>import("@/components/pages/SignUp.vue"),
        props:{
            pTitle:"SIGN UP",
            msg:"Please make sure to type your information correctly!",
        }
    },
    {
        path:'/addcert',
        name:'AddCertificate',
        component:()=>import("@/components/pages/AddCertificate.vue"),
        props:{
            pTitle:"Add New Certificate",
            msg:"Please make sure to type your information correctly!",
        }
    },
    {
        path:'/aboutme',
        name:'About Me',
        component:()=>import("@/components/pages/AboutMe.vue"),
        props:{
            pTitle:"About Me",
            msg:"My Resume",
        }
    },
    
    {
        path: "/:catchAll(.*)",
        name:'404',
        component:()=>import("@/components/pages/PageNotFound.vue"),
        props:{
            pTitle:'Uh-oh! 🤭 404 Oopsie-Whoopsie-daisy: Page Not Found! 🌌',
            msg:"Looks like we stumbled upon a cosmic hiccup! The page is playing hide-and-seek—can't be found",       
        }
    }
]

const Router = createRouter({
    history:createWebHistory(process.env.BASE_URL),
    routes
});

export default Router;
